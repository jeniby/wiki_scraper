#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv
import multiprocessing
import sys

import lxml.html as html
import requests
 

def get_html(page_url):
    """Get html of provided URL.

    Args:
        page_url(str): Page URL. 

    Returns:
        obj: object representing html of page.
    """
    html = requests.get(page_url)
    return html


def wiki_parser(wiki_page_url):
    """Get original link from wiki URL.

    Args:
        wiki_page_url(str): Wikipedia page URL. 

    Returns:
        `list` of `str`: wiki URL and original URL.
    """
    try:
        wiki_html = get_html(wiki_page_url)
    except ConnectionError:
        return [wiki_page_url, 'connection lost']

    tree = html.fromstring(wiki_html.text)
    original_url = tree.xpath('//tr[th[text() = "Website"]]//a/@href')[0]
    return [wiki_page_url, original_url] if original_url else [wiki_page__url, 'don\'t found'] 


def read_from_csv(file_name):
    """Read data from csv file.

    Args:
        file_name(str): Csv file name. 

    Returns:
        `list` of `str`: List of wiki page URL.
    """
    with open(file_name) as file:
        reader = csv.reader(file)
        data = []
        for row in reader:
            data += row
    return data


def save_to_csv(data, file_name):
    """Save data to csv file.

    Args:
        data(iter): Iterator, witch return value for writing.
        file_name(str): Csv file name.

    Returns:
        None
    """
    with open(file_name, "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerows(data)


def main():
    file_name = sys.argv[1]
    wiki_urls = read_from_csv(file_name)
    
    pool = multiprocessing.Pool(15)
    result = pool.imap(wiki_parser, wiki_urls)

    save_to_csv(result, 'answers.csv')
    

if __name__ == '__main__':
    main()